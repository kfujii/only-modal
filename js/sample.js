/**!
 * example
 *
 * @author fujii.kousuke@gmail.com
 * @license MIT
 */

$(function() {
  $('.js-open-modal').click(function(e) {
    e.preventDefault();
    var content = $('#modal-contents').html();
    $.onlyModal.open(content, '#!modal-contents');
  });

  $(document).on(
    'click',
    '.js-block',
    function() {
      if ($.onlyModal.isBlocked()) {
        return $.onlyModal.unBlock();
      }
      $.onlyModal.block();
    }
  );
});
