/**!
 * Only Modal 
 *
 * @author fujii.kousuke@gmail.com
 * @license MIT
 */

(function($, doc, w) {
  'use strict';

  $.onlyModal = {};

  var om = $.onlyModal;

  om.currentLocation = w.location.href;
  om.container = $('<div>', {'class': 'only-modal-container'})
  .css({display: 'none'});
  om.overlay = $('<div>', {'class': 'only-modal-overlay'});
  om.closeButton = $('<button>', {
    'class': 'only-modal-close',
    type: 'button'
  }).text('閉じる');
  om.content = $('<div>', {'class': 'only-modal-content'});
  om.isStatusPush = false;

  /**
   * history.pushStateのラッパー。
   * 対応しない環境では何もしない
   *
   * @param {String} url
   * @return $.onlyModal
   */
  om.pushState = function(url) {
    if (!url) {
      url = om.currentLocation;
    }

    if (w.history.pushState) {
      om.isStatusPush = true;
      var stateObj = {onlyModalState: url};
      var title = 'onlyModal';
      history.pushState(stateObj, title, url);
    }
    return om;
  };

  /**
   * @param {jQuery | HTMLElement | String} content
   * @param {String} url
   * @return jQueryElement 追加したモーダル要素
   */
  om.append = function(content, url) {
    var $container = om.container.clone();
    var $content = om.content.clone().html(content);
    var $close = om.closeButton.clone();
    var $overlay = om.overlay.clone();
    $container.append($overlay).append($content);
    $content.prepend($close);

    $('body').append($container);

    if (typeof url === 'string') {
      om.pushState(url);
    }

    return $container;
  };

  /**
   * $containerをfadeIn()するだけ
   *
   * @return $.onlyModal
   */
  om.show = function(duration, easing, callback) {
    if (duration === undefined) {
      duration = 300;
    }

    $('.only-modal-container').fadeIn(duration, easing, callback);
    $(doc).trigger('only_modal_show');
  };

  /**
   * appendとshowを続けてやる
   */
  om.open = function(content, url) {
    om.append(content, url);
    om.show();
  };

  /**
   * モーダルを閉じる
   */
  om.close = function(duration, easing, callback) {
    if (typeof callback !== 'function') {
      callback = function() {
        $('.only-modal-container').remove();
        if (om.isStatusPush) {
          om.isStatusPush = false;
          w.history.back();
        }
      };
    }

    $('.only-modal-container').fadeOut(duration, easing, callback);
    $(doc).trigger('only_modal_close');
  };

  /**
   * ブロック（ESC、外側クリックで閉じないようにする)
   */
  om.block = function() {
    $('.only-modal-container').addClass('only-modal-block');
  };

  /**
   * ブロック解除
   */
  om.unBlock = function() {
    $('.only-modal-container').removeClass('only-modal-block');
  };

  /**
   * ブロック中かどうかの判定
   */
  om.isBlocked = function() {
    return !!$('.only-modal-block').length;
  };

  /**
   * popstateイベント対応
   */
  $(w).on(
    'popstate',
    function(e) {
      om.isStatusPush = false;
      om.close();
    }
  );

  /**
   * Escキーで閉じる
   */
  $(doc).on(
    'keyup',
    function(e) {
      if (om.isBlocked()) {
        return;
      }
      if (e.keyCode === 27) {
        if ($('.only-modal-container').length) {
          om.close();
        }
      }
    }
  );

  /**
   * 外側クリックか閉じるボタンで閉じる
   */
  $(doc).on(
    'click',
    '.only-modal-overlay, .only-modal-close',
    function(e) {
      // ブロック中は外側クリックは無効
      if (om.isBlocked() && $(this).hasClass('only-modal-overlay')) {
        return;
      }
      e.preventDefault();
      om.close();
    }
  );
})(jQuery, document, window);
